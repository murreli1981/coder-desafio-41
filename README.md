# coder-desafio-41

Data Access Layer:

DAOS y Repositores

## Estructura del proyecto:

```
.
├── __test__
│   ├── __mock__
│   ├── dal
│   │   └── integration
│   ├── services
│   └── utils
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dal
│   │   ├── memory
│   │   │   ├── Dao
│   │   │   └── repositories
│   │   └── mongoose
│   │       ├── Dao
│   │       ├── Dto
│   │       ├── db
│   │       ├── models
│   │       └── repositories
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│       ├── layouts
│       └── partials
└── target
```

## Inicio del server

inicia el servidor con la persistencia de mongo
`npm start MONGO`

inicia el servidor con la persistencia en memoria
`npm start MEMORY`

### url:

`http://localhost:8080/ingreso`

## DAO

Definí los DAO para user y product, desacoplandolos para ir inyectando los modelos en cada index.js

```
src/dal.
    ├── index.js
    └── mongoose
        ├── Dao
        │   ├── index.js
        │   ├── productDao.js
        │   └── userDao.js
        └── index.js

```

Para la persistencia de tipo memory existe el mismo esquema de daos

```
src/dal
    ├── index.js
    └── memory
        ├── Dao
        │   ├── index.js
        │   ├── productDao.js
        │   └── userDao.js
        └── index.js

```

## REPOSITORIES

Se implementa un baseRepository y un nessageRepository para la entidad message, y sobreescribe el getAll() en mongo para poder traerlo con el método lean()

```
src/dal
    ├── index.js
    └── mongoose
        ├── index.js
        └── repositories
            ├── baseRepository.js
            ├── index.js
            └── messageRepository.js
```

para la persistencia de tipo memory existe el mismo esquema de repositories

```
src/dal
    ├── index.js
    └── memory
        ├── index.js
        └── repositories
            ├── baseRepository.js
            ├── index.js
            └── messageRepository.js

```

### test y coverage

los test se corren con el comando

`npm test`

que referencia a la siguiente tarea en el package.json

`"test": "jest --coverage"`

```
 PASS  __test__/services/product.test.js
  service product unit test
    ✓ getAllProducts(): it should return an array of elements when they exist (5 ms)
    ✓ getAllProducts(): the product returned should contain id, item, price and thumbnail (1 ms)
    ✓ getAllProducts() it should return an empty array when there's no result (3 ms)
    ✓ getProduct() it should return null when a given product is not found (1 ms)
    ✓ getProduct() it should return null when a non-mongo id kind is being sent
    ✓ getProduct() it should return an item with a valid id (4 ms)
    ✓ addProduct() it should return the add information when it's added (3 ms)
    ✓ updateProduct() it should return a valid object after updating (6 ms)
    ✓ deleteProduct() it should return a valid response after deleting a valid object (43 ms)
    ✓ should return undefined when model fails (63 ms)
  dal validation
    ✓ validate singleton implementation for dal (40 ms)



-----------------------------|---------|----------|---------|---------|-------------------
File                         | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
-----------------------------|---------|----------|---------|---------|-------------------
All files                    |   61.32 |    49.01 |      50 |   62.42 |
 __test__/__mock__           |     100 |      100 |     100 |     100 |
  item-found-data.js         |     100 |      100 |     100 |     100 |
  item-stored-data.js        |     100 |      100 |     100 |     100 |
  items-found-data.js        |     100 |      100 |     100 |     100 |
  no-items-found.js          |     100 |      100 |     100 |     100 |
 __test__/utils              |    93.1 |     87.5 |     100 |   92.59 |
  fake-dao.js                |    93.1 |     87.5 |     100 |   92.59 | 28,39
 src/config                  |     100 |    58.82 |     100 |     100 |
  globals.js                 |     100 |    58.82 |     100 |     100 | 9-16
 src/dal                     |   41.66 |     12.5 |     100 |      50 |
  index.js                   |   41.66 |     12.5 |     100 |      50 | 11-14
 src/dal/memory              |     100 |      100 |     100 |     100 |
  index.js                   |     100 |      100 |     100 |     100 |
 src/dal/memory/Dao          |   20.83 |        0 |    12.5 |   19.04 |
  index.js                   |     100 |      100 |     100 |     100 |
  productDao.js              |   11.53 |        0 |   11.11 |    8.33 | 3-43
  userDao.js                 |   16.66 |        0 |   14.28 |   14.28 | 3-27
 src/dal/memory/repositories |    30.3 |        0 |      20 |   26.66 |
  baseRepository.js          |   11.53 |        0 |   11.11 |    8.33 | 3-43
  index.js                   |     100 |      100 |     100 |     100 |
  messageRepository.js       |     100 |      100 |     100 |     100 |
 src/services                |     100 |      100 |     100 |     100 |
  product.js                 |     100 |      100 |     100 |     100 |
 src/utils                   |     100 |      100 |     100 |     100 |
  loggers.js                 |     100 |      100 |     100 |     100 |
-----------------------------|---------|----------|---------|---------|-------------------
```

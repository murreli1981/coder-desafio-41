const logger = require("../utils/loggers");
const log = logger.getLogger("default");
const logFile = logger.getLogger("file");
const ERR_SAVE_MSG = "SERVICE Product: Not able to store product";
const ERR_GET_MSG = "SERVICE Product: Not able to get product";

const productService = ({ productDao }) => ({
  async addProduct(product) {
    try {
      const productCreated = await productDao.create(product);
      return productCreated;
    } catch (err) {
      log.error(ERR_SAVE_MSG);
      logFile.error(`${ERR_SAVE_MSG} product: ${product.nombre} error: ${err}`);
      return null;
    }
  },

  async getProduct(id) {
    try {
      const product = await productDao.getById(id);
      return product;
    } catch (err) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + err);
      return null;
    }
  },

  async getAllProducts() {
    try {
      const products = await productDao.getAll();
      return products;
    } catch (err) {
      log.error(ERR_GET_MSG);
      logFile.error(`${ERR_GET_MSG} error: + ${err}`);
    }
  },
  async updateProduct(id, payload) {
    try {
      const productUpdated = await productDao.update(id, payload);
      return productUpdated;
    } catch (err) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + err);
      return null;
    }
  },
  async deleteProduct(id, payload) {
    try {
      const productDeleted = await productDao.delete(id);
      return productDeleted;
    } catch (err) {
      log.error(ERR_GET_MSG);
      logFile.error(ERR_GET_MSG + " id: " + id + " error: " + err);
      return null;
    }
  },
});

module.exports = productService;

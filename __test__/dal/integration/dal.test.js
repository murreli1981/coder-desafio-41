describe("dal validation", () => {
  test("validate singleton implementation for dal", async () => {
    //pido los objetos de persistencia a un factory
    const dao = require("../../../src/dal")("MEMORY");
    //agregué una property en el productDao: 'signature' que recibe un random cuando se instancia por primera vez
    const productDao = dao.productDao;
    //vuelvo a pedir nuevamente los objetos de persistencia a un factory
    const newDao = require("../../../src/dal")("MEMORY");
    //me traigo nuevamente el productDao
    const newProductDao = newDao.productDao;
    //valido que el signature es el mismo nro random generado la primera vez
    expect(productDao.signature).toBe(newProductDao.signature);
  });
});

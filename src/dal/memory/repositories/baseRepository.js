const baseRepository = (model) => ({
  async getAll() {
    const elements = model;
    console.log(elements);
    return elements;
  },

  async getById(id) {
    const element = await model.find(({ _id }) => id.toString() === _id);
    return element;
  },

  async create(element) {
    const id = model.length + 1 + "";
    const elementToCreate = {
      ...element,
      _id: id,
    };
    await model.push(elementToCreate);
    return elementToCreate;
  },

  async delete(id) {
    let elementDeleted = null;
    model = model.filter((element) => {
      if (element._id !== id.toString()) {
        return element;
      } else {
        elementDeleted = element;
      }
    });
    return elementDeleted;
  },

  async update(id, payload) {
    const elementUpdated = null;
    model = model.map((element) => {
      if (element._id === id.toString()) {
        elementUpdated = { ...element, ...payload };
        return { ...elementUpdated };
      } else return element;
    });
    return elementUpdated;
  },
});

module.exports = baseRepository;

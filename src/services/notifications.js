const nodemailer = require("nodemailer");
const moment = require("moment");
const logger = require("../utils/loggers");
const log = logger.getLogger("default");
const {
  NOTIFICATIONS: { ETHEREAL, GMAIL },
  ACCOUNT_TWILIO_SID,
  TWILIO_AUTH_TOKEN,
  ADMIN_PHONE_NUMBER,
  FROM_PHONE_NUMBER,
} = require("../config/globals");
const twilioClient = require("twilio")(ACCOUNT_TWILIO_SID, TWILIO_AUTH_TOKEN);

module.exports = class {

  constructor(mailing, texting) {
  }

  async sendMail(name, action, email, photo) {
    try {
      //registra si la acción viene de log-in o log-out y usa el nombre de usuario
      await sendFromEthereal(action, name);
      //solo dispara el mail de gmail si la cuenta es proveniente de facebook
      if (action === "Log-in" && email && photo) {
        try {
          sendFromGmail(email, photo, name);
        } catch (error) {
          //loguea en caso de que falle
          log.error(
            `GMAIL notification failed with email: [${email}] ${error}`
          );
        }
      }
    } catch (error) {
      //loguea en caso de que falle
      log.error(
        `ETHEREAL notification failed with email [${email}] - ${error}`
      );
    }
  }

  async sendSMS(name, message) {
    try {
      sendFromTwilio(name, message);
    } catch (error) {
      //loguea en caso de que falle
      log.error(
        `TWILIO notification failed with phone ${ADMIN_PHONE_NUMBER} - ${error}`
      );
    }
  }
};

//Metodo que dispara el sms via twilio
const sendFromTwilio = async (name, message) => {
  await twilioClient.messages.create({
    from: FROM_PHONE_NUMBER,
    to: ADMIN_PHONE_NUMBER,
    body: `[${name}] ${message}`,
  });
};

//Metodo que dispara el email desde Ethereal de/hacia una cuenta administrador
const sendFromEthereal = async (action, name) => {
  const transporter = nodemailer.createTransport(ETHEREAL);
  await transporter.sendMail({
    from: '"[Mailer]" do-not-reply@emailnotification.com',
    to: "isai.quigley2@ethereal.email",
    subject: `${action} - ${name}`,
    html: `[${moment().format("DD/MM/YYYY hh:mm:ss")}] - ${name}`,
  });
};

//Metodo que dispara el email desde Gmail de/hacia una cuenta de usuario proveniente de facebook
const sendFromGmail = async (email, photo, name) => {
  const transporterGmail = nodemailer.createTransport(GMAIL);
  await transporterGmail.sendMail({
    from: '"[Mailer]" do-not-reply@emailnotification.com',
    to: email,
    subject: `Welcome ${name} from your Facebook account!`,
    html: `<img src=${photo}/>`,
    attachments: [
      { path: photo, filename: "foto.jpeg", contentType: "image/jpeg" },
    ],
  });
};

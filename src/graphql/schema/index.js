const { buildSchema } = require("graphql");

module.exports = buildSchema(`
    type Product {
        _id: ID!
        title: String!
        price: String!
        thumbnail: String!
    }

    input ProductInput {
        title: String!
        price: String!
        thumbnail: String!
    }

    type Query {
        products: [Product!]
        product(id: ID!): Product
    }

    type Mutation {
        createProduct(product:ProductInput): Product
    }

    schema {
        query: Query
        mutation: Mutation
    }
`);

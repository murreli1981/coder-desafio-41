module.exports = class {
  _info = {
    "Argumentos de entrada": process.argv.slice(2),
    "Sistema Operativo": process.platform,
    "Numero de CPUs": require("os").cpus().length,
    "Version de NodeJS": process.version,
    "Uso de memoria": process.memoryUsage(),
    "Path de ejecución": process.execPath,
    "Process Id": process.pid,
    "Carpeta corriente": process.cwd(),
  };

  getNumCPUs = function () {
    return require("os").cpus().length;
  };

  getProcessInfo = function () {
    return this._info;
  };
};

const models = require("../../../../__test__/__mock__/init-memory.json");
const messageRepository = require("./messageRepository");

module.exports = {
  messageRepository: messageRepository(models),
};

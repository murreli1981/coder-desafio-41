const baseRepository = require("./baseRepository");

const messageRepository = ({ messageModel }) => ({
  ...baseRepository(messageModel),
  async getAll() {
    const messages = await messageModel.find().lean();
    return messages;
  },
});

module.exports = messageRepository;

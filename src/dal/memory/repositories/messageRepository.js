const baseRepository = require("./baseRepository");

const messageRepository = ({ messageModel }) => ({
  ...baseRepository(messageModel),
});

module.exports = messageRepository;

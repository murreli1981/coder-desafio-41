const productDao = ({ productModel }) => ({
  async getAll() {
    const products = productModel;
    return products;
  },

  async getById(id) {
    const product = productModel.find(({ _id }) => id.toString() === _id);
    return product;
  },

  async create(product) {
    const id = productModel.length + 1 + "";
    const productToCreate = {
      ...product,
      _id: id,
    };
    productModel.push(productToCreate);
    return productToCreate;
  },

  async delete(id) {
    let productDeleted = null;
    productModel = productModel.filter((item) => {
      if (item._id !== id.toString()) {
        return item;
      } else {
        console.log(`found! ${item._id}`);
        productDeleted = item;
      }
    });
    return productDeleted;
  },

  async update(id, payload) {
    let productUpdated = null;
    productModel = await productModel.map((item) => {
      if (item._id === id.toString()) {
        productUpdated = { ...item, ...payload };
        return { ...productUpdated };
      } else return item;
    });
    return productUpdated;
  },
  signature: Math.random(),
});

module.exports = productDao;
